#!/usr/bin/env bash
# termux-utils/color-util

COLORPATH=$HOME/.termux/colors

paint_color() {
    $NE "\e[2J\e[0;0H"
    $NE "$(cpattern 1 92 104)Termux Utils"
    fit -12
    $NE "$(cpattern 0 33 46)Termux 美化工具"
    fit -15
    $EE "$(cpattern 0 0 0)"
    $NE "$(cpattern 3 30 43)请选择配色方案（按 [“v”+序号] 可以预览配色方案，按 [“a”+序号] 可以应用配色方案）"
    fit -80
    $NE "$(cpattern 0 0 0)"
    local count=0
    for color in $COLORPATH/*; do
        color_file[count]=$color
        $EE "    $count\t$($EC ${color_file[count]} | awk -F'/' '{print $NF}')"
        count=$(($count + 1))
    done
    count=$(($count - 1))
    $EE "    q\t返回"
}


paint_color
ch=""
until [ "$ch" == "q" ]; do
    read -sN1 ch
    case $ch in
        v)
            read -p"请输入序号：" co
            termux-open "${color_file[co]}.png";;
        a)
            read -p"请输入序号：" co
            ln -sf "${color_file[co]}" ~/.termux/colors.properties
            termux-reload-settings
            paint_color;;
    esac
done
ch=""

