#!/usr/bin/env bash
# termux-utils/font-util

FONTPATH=$HOME/.termux/fonts

paint_font() {
    $NE "\e[2J\e[0;0H"
    $NE "$(cpattern 1 92 104)Termux Utils"
    fit -12
    $NE "$(cpattern 0 33 46)Termux 美化工具"
    fit -15
    $EE "$(cpattern 0 0 0)"
    $NE "$(cpattern 3 30 43)请选择字体（按 [“v”+序号] 可以预览字体，按 [“a”+序号] 可以应用字体）"
    fit -68
    $NE "$(cpattern 0 0 0)"
    local count=0
    for font in $FONTPATH/{*.ttf,*.otf}; do
        font_file[count]=$font
        $EE "    $count\t$($EC ${font_file[count]} | awk -F'/' '{print $NF}')"
        count=$(($count + 1))
    done
    count=$(($count - 1))
    $EE "    q\t返回"
}


paint_font
ch=""
until [ "$ch" == "q" ]; do
    read -sN1 ch
    case $ch in
        v)
            read -p"请输入序号：" co
            termux-open "${font_file[co]}.png";;
        a)
            read -p"请输入序号：" co
            ln -sf "${font_file[co]}" ~/.termux/font.ttf
            termux-reload-settings
            paint_font;;
    esac
done
ch=""

