#!/usr/bin/env bash
# termux-utils

env > /dev/null

EC=echo
EE='echo -e'
NN='echo -n'
NE='echo -ne'

cpattern() {
	# $1 - style
	# $2 - fgcolor
	# $3 - bgcolor
	$EC "\\e[$1;$2;$3m"
}

fit() {
    local i=0
    target=$(($COLUMNS + $1))
    if [ $target -lt 1 ]; then
        $EE "\n$(cpattern 1 31 43)错误: 屏幕行列数太小，显示失败！$(cpattern 0 0 0)"
        exit 1
    fi
    until [ $i -ge $target ]; do
        $NN " "
        ((i++))
    done
    $EC
}

paint_main() {
    $NE "\e[2J\e[0;0H"
    $NE "$(cpattern 1 92 104)Termux Utils"
    fit -12
    $NE "$(cpattern 0 33 46)Termux 美化工具"
    fit -15
    $EE "$(cpattern 0 0 0)"
    $NE "$(cpattern 3 30 43)请选择你想要调整的项目"
    fit -22
    $EE "$(cpattern 0 0 0)    1 安装字体"
    $EE "$(cpattern 0 0 0)    2 安装配色"
    $EE "$(cpattern 0 0 0)    3 自定义配色"
	$EE "$(cpattern 0 0 0)    4 双排键盘"
    $EE "$(cpattern 0 0 0)    q 退出"
}

$NE "\e[$((${LINES} - 1))S"

paint_main
ch=""
until [ "$ch" == "q" ]; do
    read -sN1 ch
    case $ch in
        1)
            . font-util.sh
            paint_main;;
        2)
            . color-util.sh
            paint_main;;
        4)
            if [ -e $HOME/.termux/termux.properties ]; then
                $NE "\e[2J\e[0;0H"
                $NE "$(cpattern 1 33 106)警告"
                fit -4
                $NE "$(cpattern 1 33 0)检测到已有配置文件，配置文件中可能"
                $EE "存有内容。如果进行默认操作，可能会造成原有配置丢失。"
                $EE "$(cpattern 0 0 0)"
                $NE "$(cpattern 3 30 43)请选择你想要进行的操作"
                fit -22
                $EE "$(cpattern 0 0 0)    o 直接覆盖"
                $EE "$(cpattern 0 0 0)    b 备份原配置文件并覆盖"
                $EE "$(cpattern 0 0 0)    a 追加到原配置文件"
                $EE "$(cpattern 0 0 0)    q 取消"
                read -sN1 op
                case $op in
                    b|o)
                        if [ $op == "b" ]; then
                            cp ~/.termux/termux.properties ~/.termux/termux.properties.$(date +"%Y-%m-%d-%H-%M-%S")
                        fi
                        echo "extra-keys = [['ESC','|','/','HOME','UP','END','PGUP','DEL'], ['TAB','CTRL','ALT','LEFT','DOWN','RIGHT','PGDN','BKSP']]" > $HOME/.termux/termux.properties
                        ;;
                    a)
                        echo "extra-keys = [['ESC','|','/','HOME','UP','END','PGUP','DEL'], ['TAB','CTRL','ALT','LEFT','DOWN','RIGHT','PGDN','BKSP']]" >> $HOME/.termux/termux.properties
                        ;;
                esac
            fi
            if [ $op != "q" ]; then
                $EE "$(cpattern 5 92 104)完成！重启程序以完整应用更改"
                $NE "[按任意键继续]$(cpattern 0 0 0)"
                read -sN1
            fi
            $NE "\e[2J\e[0;0H"
            paint_main
            ;;
    esac
done
$NE "\e[2J\e[0;0H"

