## deb包标准：
### 命名：
* 每个deb包都要按照类型添加前缀，参考下表：

| 类型 | 前缀 |
|:----:|:----:|
| 工具 | toktools |
| proot或chroot系统 | toksystem |
| 美化 | toktheme |
| 其他 | tokother |
 
 * 除了前缀外，还有些工具软件包需要`tag`，参考下表：

 | 大类 | tag类型 | tag |
 |:----:|:----:|:----:|
 | 工具 | 媒体 | media |
 | 工具 |系统优化 | sysopt |
 | 工具 | 测试 | test |
 | 工具 | 脚本 | script |
 | 其他 | 不限 | 自定 |

 我们的deb包名格式为**前缀+[tag]+软件名**  
 假设我有一个叫做speedtest的包用来测试网速，那么它的包名就应该是`toktools-test-speedtest`
 ## 版本号
 软件包的版本号，应该与软件上游发布的版本号一致。变量的值可以由字母、数字和英文句点 .，连字符 - 组成，但不能包含下划线(_)。如果上游版本号中使用了下划线，则应该用连字符 - 来替代。
 # 发布
 你需要
 1.注册github/gitee账号  
 2.向[仓库](https://github.com/toknife/apt-repo-deb)或[gitee仓库](https://gitee.com/the_lowest_level/apt-repo-deb)提交你的Pull Request  
 3.等待审核即可上架  
